package com.suiyun.common.util;


import com.suiyun.common.enums.ReturnCodeEnum;
import com.suiyun.common.exception.ParameterException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;


@Slf4j
public class CheckDataUtils {

    /**
     * 判断参数是否为空，为空则抛出参数异常
     *
     * @param args
     */
    @SafeVarargs
    public static <T> void judgeNull(T... args) throws ParameterException {
        if (args == null || args.length == 0) {
            throw new ParameterException(ReturnCodeEnum.PARAMETER_ERROR);
        }

        for (T item : args) {
            if (item == null) {
                throw new ParameterException(ReturnCodeEnum.PARAMETER_ERROR);
            }

            if (item instanceof String) {
                if (StringUtils.isBlank((String) item)) {
                    throw new ParameterException(ReturnCodeEnum.PARAMETER_ERROR);
                }
            }

        }
    }

}

package com.suiyun.common.exception;


import com.suiyun.common.enums.ReturnCodeEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class ParameterException extends RuntimeException {


    private static final long serialVersionUID = -3350326695109352222L;
    private final Integer code;

    public ParameterException(ReturnCodeEnum r) {
        super(r.getMsg(), null, false, true);
        this.code = r.getCode();
    }


}

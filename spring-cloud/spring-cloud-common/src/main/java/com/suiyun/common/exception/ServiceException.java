package com.suiyun.common.exception;


import com.suiyun.common.enums.ReturnCodeEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class ServiceException extends RuntimeException {


    private static final long serialVersionUID = -6078494910627702671L;
    private final Integer code;

    private final String msg;

    public ServiceException(ReturnCodeEnum r) {
        this(r.getCode(), r.getMsg());
    }

    public ServiceException(Integer code, String msg) {
        super(msg, null, false, false);
        this.code = code;
        this.msg = msg;
    }

    public ServiceException(String msg) {
        this(ReturnCodeEnum.ERROR.getCode(), msg);
    }


}

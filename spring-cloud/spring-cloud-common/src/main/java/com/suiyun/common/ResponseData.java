package com.suiyun.common;


import lombok.Data;

/**
 * 封装JSON数据
 **/
@Data
public class ResponseData<T> {

    private Boolean success;
    private Integer code;
    private String msg;
    private T data;

    public ResponseData() {}

    public ResponseData(Integer code, String msg, T data,Boolean success) {
        this.code = code;
        this.msg = msg;
        this.data = data;
        this.success = success;
    }

    public ResponseData(T data) {
        this.data = data;
    }

}

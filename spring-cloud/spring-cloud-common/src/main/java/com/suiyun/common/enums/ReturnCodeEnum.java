package com.suiyun.common.enums;

/**
 * 接口返回码和返回值
 * 结合ResponseUtils 使用
 **/
public enum ReturnCodeEnum {
    /**
     * 成功
     */
    SUCCE(1, "操作成功"),
    /**
     * 失败
     */
    FAIL(0, "操作失败"),
    /**
     * 对象为空
     */
    FAIL_OBJECTISNULL(3, "对象为空"),
    /**
     * 数据不存在返回
     */
    NOT_FOUND(-1, "notFound [数据不存在 或者 数据为空]"),
    /**
     * 异常返回
     */
    ERROR(-1, "error [系统异常或无此权限]"),

    /**
     * 参数有异常返回
     */
    PARAMETER_ERROR(0, "parameter error [参数异常:参数为空或者参数类型不符]"),

    FORBIDDEN(403,"forbidden 服务器理解请求客户端的请求，但是拒绝执行此请求");

    private Integer code;
    private String msg;

    ReturnCodeEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }


}

package com.suiyun.common.util;


import com.alibaba.fastjson.JSONObject;
import com.suiyun.common.ResponseData;
import com.suiyun.common.enums.ReturnCodeEnum;
import com.suiyun.common.exception.ServiceException;
import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * json返回结果封装
 */
public class ResponseUtils implements java.io.Serializable {

	private static final long serialVersionUID = 4228518756396938386L;

	public static ResponseData success(Object data) {
		return new ResponseData(ReturnCodeEnum.SUCCE.getCode(), ReturnCodeEnum.SUCCE.getMsg(), data,Boolean.TRUE);
	}

	public static ResponseData success(String msg, Object data) {
		return new ResponseData(ReturnCodeEnum.SUCCE.getCode(), msg, data,Boolean.TRUE);
	}

	public static ResponseData success(Integer code, String msg, Object data) {
		return new ResponseData(code, msg, data,Boolean.TRUE);
	}


	public static ResponseData failed(Object data) {
		return new ResponseData(ReturnCodeEnum.FAIL.getCode(), ReturnCodeEnum.FAIL.getMsg(), data,Boolean.FALSE);
	}

	public static ResponseData failed() {
		return new ResponseData(ReturnCodeEnum.FAIL.getCode(), ReturnCodeEnum.FAIL.getMsg(), null,Boolean.FALSE);
	}

	public static ResponseData failed(Integer code, String msg) {
		return new ResponseData(code, msg, null,Boolean.FALSE);
	}

	public static ResponseData failed(String msg) {
		return new ResponseData(ReturnCodeEnum.FAIL.getCode(), msg, null,Boolean.FALSE);
	}

	public static ResponseData failed(String msg, Object data) {
		return new ResponseData(ReturnCodeEnum.FAIL.getCode(), msg, data,Boolean.FALSE);
	}


	public static ResponseData failedParam(Object data) {
		return new ResponseData(ReturnCodeEnum.PARAMETER_ERROR.getCode(), ReturnCodeEnum.PARAMETER_ERROR.getMsg(), data,Boolean.FALSE);
	}

	public static ResponseData failedObjectIsNull(Object data) {
		return new ResponseData(ReturnCodeEnum.FAIL_OBJECTISNULL.getCode(), ReturnCodeEnum.FAIL_OBJECTISNULL.getMsg(), data,Boolean.FALSE);
	}

	public static ResponseData failedNoData(Object data) {
		return new ResponseData(ReturnCodeEnum.NOT_FOUND.getCode(), ReturnCodeEnum.NOT_FOUND.getMsg(), data,Boolean.FALSE);
	}

	public static ResponseData successFormat(Object data) {
		return new ResponseData(data);
	}

	/**
	 * 未找到资源
	 * @param msg
	 * @return
	 */
	public static ResponseData notFoundResource(String msg){
		return new ResponseData(HttpStatus.NOT_FOUND.value(),msg,null,Boolean.FALSE);
	}

	/**
	 * 系统异常
	 */
	public static ResponseData systemError() {
		return new ResponseData(ReturnCodeEnum.ERROR.getCode(),ReturnCodeEnum.ERROR.getMsg(),null,Boolean.FALSE);
	}


	public static <T> T orError(ResponseData<T> resp) {
		if (resp.getSuccess()) {
			return resp.getData();
		} else {
			throw new ServiceException(ReturnCodeEnum.ERROR);
		}
	}

	/**
	 * 使用response输出JSON
	 *
	 * @param response
	 * @param res
	 */
	public static void out(HttpServletResponse response, String res) {

		response.setContentType("application/json;charset=UTF-8");

		response.setHeader("Cache-Control","no-store, max-age=0, no-cache, must-revalidate");
		response.addHeader("Cache-Control", "post-check=0, pre-check=0");
		response.setHeader("Pragma", "no-cache");
		response.setStatus(JSONObject.parseObject(res).getInteger("code"));
        /*//统一用过滤器设置跨域
        response.setHeader("Access-Control-Allow-Methods", "POST,GET,OPTIONS,DELETE,PUT");
        response.setHeader("Access-Control-Max-Age", "3600");
        response.setHeader("Access-Control-Allow-Headers", "x-requested-with,Authorization");
        response.setHeader("Access-Control-Allow-Credentials","true");*/
		try {
			PrintWriter out = response.getWriter();
			out.write(res);
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}

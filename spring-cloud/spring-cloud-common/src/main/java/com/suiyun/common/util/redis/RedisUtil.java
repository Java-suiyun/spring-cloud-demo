package com.suiyun.common.util.redis;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.*;
import java.util.concurrent.TimeUnit;

@Component
public final class RedisUtil {

    private static RedisTemplate<Serializable, Object> redisTemplate;

    /**
     * 批量删除对应的value
     *
     * @param keys
     */
    public void remove(final String... keys) {
        for (String key : keys) {
            remove(key);
        }
    }

    /**
     * 批量删除key
     *
     * @param pattern
     */
    public void removePattern(final String pattern) {
        Set<Serializable> keys = redisTemplate.keys(pattern);
        if (keys.size() > 0){
            redisTemplate.delete(keys);
        }
    }

    /**
     * 删除对应的value
     *
     * @param key
     */
    public void remove(final String key) {
        if (exists(key)) {
            redisTemplate.delete(key);
        }
    }

    /**
     * 判断缓存中是否有对应的value
     *
     * @param key
     * @return
     */
    public boolean exists(final String key) {
        return redisTemplate.hasKey(key);
    }

    /**
     * 读取缓存
     *
     * @param key
     * @return
     */
    public Object get(final String key) {
        Object result;
        ValueOperations<Serializable, Object> operations = redisTemplate.opsForValue();
        result = operations.get(key);
        return result;
    }

    /**
     * 写入缓存
     *
     * @param key
     * @param value
     * @return
     */
    public boolean set(final String key, Object value) {
        boolean result = false;
        try {
            ValueOperations<Serializable, Object> operations = redisTemplate.opsForValue();
            operations.set(key, value);
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 写入缓存
     *
     * @param key
     * @param value
     * @return
     */
    public boolean set(final String key, Object value, Long expireTime) {
        boolean result = false;
        try {
            ValueOperations<Serializable, Object> operations = redisTemplate.opsForValue();
            operations.set(key, value);
            redisTemplate.expire(key, expireTime, TimeUnit.SECONDS);
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public boolean set(final String key, Object value, Integer expireTime,TimeUnit timeUnit) {
        boolean result = false;
        try {
            ValueOperations<Serializable, Object> operations = redisTemplate.opsForValue();
            operations.set(key, value);
            redisTemplate.expire(key, expireTime, timeUnit);
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public boolean set(final String key, Object value, Date expire) {
        boolean result = false;
        try {
            ValueOperations<Serializable, Object> operations = redisTemplate.opsForValue();
            operations.set(key, value);
            redisTemplate.expireAt(key, expire);
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public boolean increment(final String key, double value, Date expireAt) {
        boolean result = false;
        try {
            ValueOperations<Serializable, Object> operations = redisTemplate.opsForValue();
            operations.increment(key, value);
            if (expireAt != null) {
                redisTemplate.expireAt(key, expireAt);
            }
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public String getLockValue(long expire) {
        Long value = System.currentTimeMillis() + expire * 1000L;
        return value.toString();
    }

    public String getLockValue() {
        return this.getLockValue(5L);
    }

    /**
     * 建议使用 getLock 方法代替
     * 原因有1
     * 1. redis key尽量都加上过期日期，不然key多了很难维护
     *
     * @param key
     * @param value
     * @return
     */
    @Deprecated
    public boolean getlLock(String key, String value) {
        if (redisTemplate.opsForValue().setIfAbsent(key, value)) {
            return true;
        } else {
            String currentValue = (String) redisTemplate.opsForValue().get(key);
            if (!StringUtils.isEmpty(currentValue) && Long.parseLong(currentValue) < System.currentTimeMillis()) {
                String oldValue = (String) redisTemplate.opsForValue().getAndSet(key, value);
                return StringUtils.isNotEmpty(oldValue) && oldValue.equals(currentValue);
            } else {
                return false;
            }
        }
    }

    /**
     * 获得redis原子性操作锁，默认3秒
     *
     * @param key
     * @return
     */
    public boolean getLock(String key) {
        return getLock(key, 3);
    }

    /**
     * 获得redis原子性操作锁
     * 1. 如果获取成功，返回成功 && 加上过期时间
     *
     * @param key          锁的KEY
     * @param expireSecond 锁的时长，单位:秒
     * @return
     */
    public boolean getLock(String key, long expireSecond) {
        Boolean flag = redisTemplate.opsForValue().setIfAbsent(key, 1);
        // 如果获得锁成功，直接返回
        if (Objects.nonNull(flag) && flag) {
            // 若在这里程序突然崩溃，则无法设置过期时间，将发生死锁
            redisTemplate.expire(key, expireSecond, TimeUnit.SECONDS);
            return true;
        }
        return false;
    }

    /**
     * @param key
     * @param value
     * @param expireAt
     * @return
     */
    public Double inc(final String key, double value, Date expireAt) {
        ValueOperations<Serializable, Object> operations = redisTemplate.opsForValue();
        Double result = operations.increment(key, value);
        if (expireAt != null) {
            redisTemplate.expireAt(key, expireAt);
        }
        return result;
    }

    /**
     * 递增或递减
     *
     * @param key   键
     * @param delta 正数表示加几,负数表示减几
     * @return 加减完后的值
     */
    public Long incr(String key, long delta, Date expireAt) {
        Long result = redisTemplate.opsForValue().increment(key, delta);
        if (expireAt != null) {
            redisTemplate.expireAt(key, expireAt);
        }
        return result;
    }

    public Set keys(String pattern) {
        return redisTemplate.keys(pattern);
    }

    /**
     * 获取key的过期时间
     *
     * @param key 过期时间
     * @return 过期时间的秒数
     */
    public long expire(String key) {
        return redisTemplate.getExpire(key);
    }

    /**
     * 判断一个key是否已过期
     *
     * @param key
     * @return true-已过期,false-没过期
     */
    public boolean isExpire(String key) {
        Long expire = redisTemplate.getExpire(key, TimeUnit.SECONDS);
        return Objects.isNull(expire) || expire <= 0;
    }

    /**
     * 修改key的过期时间
     *
     * @param key 过期时间
     * @return 过期时间的秒数
     */
    public Boolean expireAt(String key, Date expireAt) {
        return redisTemplate.expireAt(key, expireAt);
    }

    /**
     * 从队列最后(最右侧)位置取出值
     *
     * @param key
     * @return
     */
    public Object pop(String key) {
        return redisTemplate.boundListOps(key).rightPop();
    }

    /**
     * 从队列开始(最左侧)位置插入值
     *
     * @param key
     * @return
     */
    public Object push(String key, Object value) {
        return redisTemplate.boundListOps(key).leftPush(value);
    }

    /**
     * 从队列开始(最左侧)位置插入值，同时对key的有效期进行更新
     *
     * @param key
     * @return
     */
    public Boolean push(String key, Object value, Date expireAt) {
        redisTemplate.boundListOps(key).leftPush(value);
        return redisTemplate.boundListOps(key).expireAt(expireAt);
    }

    /**
     * setter方法注入RedisTemplate
     *
     * @param redisTemplate 对象
     */
    public void setRedisTemplate(RedisTemplate<Serializable, Object> redisTemplate) {
        RedisUtil.redisTemplate = redisTemplate;
    }

    /**
     * 递增Hash（类似Java Map） 类型的某个键 对应的值
     * <p>
     * 如：key{
     * hashKey : value
     * }
     * 调用这个方法，则递增 value 的值
     *
     * @param key          redis 对应的 键
     * @param hashKey      hash 的某个 键
     * @param incrementNum 递增的量
     * @return 增量过后的值
     */
    public Long hashIncrement(String key, Object hashKey, long incrementNum) {
        return redisTemplate.opsForHash().increment(key, hashKey, incrementNum);
    }

    /**
     * 获取Hash 中的某个键对应的值
     * 如：key{
     * hashKey : value
     * }
     * 调用此方法，则获取到hashKey 对应的value
     *
     * @param key     redis 键
     * @param hashKey hash 的某个键
     * @return
     */
    public Object hashGet(String key, String hashKey) {
        return redisTemplate.opsForHash().get(key, hashKey);
    }

    /**
     * 将整个Map 放入一个键对应的 Hash（Map）中
     * 如：key{
     * hashKey : value
     * }
     * 调用此方法，则将map 中的对应键值数据放入key这个Map中，类似 JAVA 中 MAP 集合的 putAll()方法
     *
     * @param key redis 键
     * @param map 要放入的map 集合
     */
    public void hashPutAll(String key, Map<String, Object> map) {
        redisTemplate.opsForHash().putAll(key, map);
    }

    /**
     * 将某个值放入 对应 Hash
     * 如：key{
     * hashKey : value
     * }
     * 调用此方法，则将 hashKey 和 hashValue 放入，类似 JAVA中 MAP 集合的 put() 方法
     *
     * @param key       redis 对应的 键
     * @param hashKey   hash 中的 键
     * @param hashValue hash 中的 值
     */
    public void hashPut(String key, String hashKey, Object hashValue) {
        redisTemplate.opsForHash().put(key, hashKey, hashValue);
    }

    /**
     * 批量获取Hash 中的多个值
     * 如：key{
     * key1 : value1,
     * key2 : value2
     * key3 : value3
     * }
     * 如果参数为 key,[key1,key2]，则返回 List [value1,value2]
     * 如果参数为 key,[key1,key10]，则返回 List [value1,null]
     *
     * @param key     redis 键
     * @param keyList key 的集合
     */
    public List<Object> hashMultiGet(String key, List<Object> keyList) {
        return redisTemplate.opsForHash().multiGet(key, keyList);
    }

    /**
     * 获取整个Hash 的值
     * 如：key{
     * key1 : value1,
     * key2 : value2
     * key3 : value3
     * }
     * 调用此方法，则返回整个key 对应的Map
     *
     * @param key redis 键
     */
    public Map<Object, Object> hashEntries(String key) {
        return redisTemplate.opsForHash().entries(key);
    }

    /**
     * 删除hash中field这一对kv
     *
     * @param key
     * @param field
     */
    public void hashDel(String key, String field) {
        redisTemplate.opsForHash().delete(key, field);
    }

    /**
     * hasKey方法，field键是否存在
     *
     * @param key
     * @param field
     */
    public Boolean hasHashKey(String key, String field) {
        return redisTemplate.opsForHash().hasKey(key, field);
    }

}

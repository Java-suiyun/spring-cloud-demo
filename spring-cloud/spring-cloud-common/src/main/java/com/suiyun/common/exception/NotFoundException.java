package com.suiyun.common.exception;


import com.suiyun.common.enums.ReturnCodeEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class NotFoundException extends RuntimeException {

    private static final long serialVersionUID = -1585131588120960685L;
    private final Integer code;

    public NotFoundException(ReturnCodeEnum r) {
        super(r.getMsg(), null, false, false);
        this.code = r.getCode();

    }

}

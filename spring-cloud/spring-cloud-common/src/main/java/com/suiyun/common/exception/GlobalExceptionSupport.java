package com.suiyun.common.exception;


import com.suiyun.common.ResponseData;
import com.suiyun.common.enums.ReturnCodeEnum;
import com.suiyun.common.util.ResponseUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;


@ControllerAdvice
@Slf4j
@ResponseBody
public class GlobalExceptionSupport {

    @ExceptionHandler(NotFoundException.class)
    public ResponseData handleNotFoundException(NotFoundException e) {
        return ResponseUtils.notFoundResource(e.getMessage());
    }


    @ExceptionHandler(ParameterException.class)
    public ResponseData handleParameterException(HttpServletRequest req, ParameterException e) {
        log.error("接口{}缺少必要参数异常", req.getRequestURI());
        return ResponseUtils.failed(ReturnCodeEnum.PARAMETER_ERROR.getCode(), ReturnCodeEnum.PARAMETER_ERROR.getMsg());
    }


    @ExceptionHandler(ServiceException.class)
    public ResponseData handleServiceException(HttpServletRequest req, ServiceException e) {
        log.info("接口{},提示=={}", req.getRequestURI(), e.getMsg());
        return ResponseUtils.failed(e.getCode(), e.getMsg());
    }



    @ExceptionHandler(value = Exception.class)
    public ResponseData handleCommonException(HttpServletRequest req, Exception e) {
        log.error("接口{}出现异常", req.getRequestURI());
        return ResponseUtils.systemError();
    }


}
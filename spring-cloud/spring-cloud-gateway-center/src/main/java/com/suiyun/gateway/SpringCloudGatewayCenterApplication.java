package com.suiyun.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class SpringCloudGatewayCenterApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringCloudGatewayCenterApplication.class, args);
	}

}

package com.suiyun.product.controller;

import com.suiyun.common.ResponseData;
import com.suiyun.common.util.ResponseUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ww
 */
@RestController
public class ProductTestController {

    @Value("${server.port}")
    private String port;

    @GetMapping("/product")
    public ResponseData<String> product(){
        return ResponseUtils.success( "Hello！I'm product. port：" + port);
    }
}

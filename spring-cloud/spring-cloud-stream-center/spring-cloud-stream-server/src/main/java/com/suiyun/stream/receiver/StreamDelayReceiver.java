package com.suiyun.stream.receiver;

import com.suiyun.stream.client.StreamDelayClient;
import com.suiyun.stream.constant.StreamConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Component;

/**
 * @author ww
 * @date 2020/9/28 上午11:29
 */
@Slf4j
@Component
@EnableBinding(value = {StreamDelayClient.class})
public class StreamDelayReceiver {

    @StreamListener(StreamConstant.INPUT_DELAY)
    public void receive(String message) {
        log.info("StreamDelayReceiver: {}", message);
    }
}

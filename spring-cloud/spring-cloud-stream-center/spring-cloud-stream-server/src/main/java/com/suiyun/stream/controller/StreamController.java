package com.suiyun.stream.controller;

import com.suiyun.stream.client.StreamClient;
import com.suiyun.stream.client.StreamDelayClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ww
 * @date 2020/9/28 上午11:55
 */
@Slf4j
@RestController
public class StreamController {

    @Autowired
    private StreamClient streamClient;

    @Autowired
    private StreamDelayClient streamDelayClient;


    @GetMapping("/send")
    public void send() {
        streamClient.output().send(MessageBuilder.withPayload("Hello World...").build());
        log.info("send发送成功");
    }

    @GetMapping("/sendDelay")
    public void sendDelay() {
        streamDelayClient.output().send(MessageBuilder.withPayload("Hello World...").setHeader("x-delay",5000).build());
        log.info("sendDelay发送成功");
    }
}

package com.suiyun.stream;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class SpringCloudStreamCenterApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringCloudStreamCenterApplication.class, args);
	}

}

package com.suiyun.stream.client;

import com.suiyun.stream.constant.StreamConstant;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

/**
 * @author ww
 * @date 2020/9/28 上午11:26
 */
public interface StreamClient {

    @Input(StreamConstant.INPUT)
    SubscribableChannel input();

    @Output(StreamConstant.OUTPUT)
    MessageChannel output();
}

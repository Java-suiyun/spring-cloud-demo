package com.suiyun.stream.client;

import com.suiyun.stream.constant.StreamConstant;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

/**
 * @author ww
 * @date 2020/9/28 下午3:30
 */
public interface StreamDelayClient {
    @Input(StreamConstant.INPUT_DELAY)
    SubscribableChannel input();

    @Output(StreamConstant.OUTPUT_DELAY)
    MessageChannel output();
}

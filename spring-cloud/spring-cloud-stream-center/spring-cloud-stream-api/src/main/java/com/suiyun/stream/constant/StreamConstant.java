package com.suiyun.stream.constant;

/**
 * @author ww
 * @date 2020/9/28 上午11:23
 */
public class StreamConstant {

    public static final String INPUT = "myInput";
    public static final String OUTPUT = "myOutput";

    public static final String INPUT_DELAY = "myInputDelay";
    public static final String OUTPUT_DELAY = "myOutputDelay";

}

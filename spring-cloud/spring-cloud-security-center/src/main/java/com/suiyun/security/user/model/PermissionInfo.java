package com.suiyun.security.user.model;

import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author ww
 * @since 2020-09-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("uc_permission_info")
public class PermissionInfo implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 权限id
     */
    private Long id;

    /**
     * 权限名
     */
    private String name;

    /**
     * 接口路径
     */
    private String path;

    /**
     * 说明
     */
    private String description;

    /**
     * 开启 1, 关闭0
     */
    private Integer status;

    /**
     * 请求方式 GET POST PUT DELETE
     */
    private String method;

    private LocalDateTime createdAt;

    private LocalDateTime updatedAt;


}

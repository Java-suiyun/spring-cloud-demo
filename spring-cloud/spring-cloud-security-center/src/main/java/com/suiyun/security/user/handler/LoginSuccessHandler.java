//package com.suiyun.security.user.handler;
//
//
//import cn.hutool.json.JSONUtil;
//import com.suiyun.common.util.ResponseUtils;
//import com.suiyun.common.util.redis.RedisUtil;
//import com.suiyun.security.user.dto.AuthUserDto;
//import com.suiyun.security.user.util.TokenUtil;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
//import org.springframework.stereotype.Component;
//
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//import java.util.HashMap;
//import java.util.Map;
//import java.util.concurrent.TimeUnit;
//
///**
// * 登陆认证成功处理过滤器
// * @author ww
// * @date 2020/9/23 下午4:00
// */
//
//@Component
//public class LoginSuccessHandler implements AuthenticationSuccessHandler {
//    @Autowired
//    private TokenUtil tokenUtil;
//
//    @Autowired
//    private RedisUtil redisUtil;
//
//    /**
//     * 用户认证成功后 生成token并返回
//     **/
//    @Override
//    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
//
//        AuthUserDto authUserDetails = (AuthUserDto) authentication.getPrincipal();//从内存中获取当前认证用户信息
//        String Username = authentication.getName();
//
//        //在redis中查询用户之前是否登入
//        Object oldToken = redisUtil.get("token_" + Username);
//        if (oldToken != null) {
//            //清除旧Token
//            redisUtil.remove("token_" + Username);
//        }
//
//        //创建token
//        String accessToken = tokenUtil.createAccessJwtToken(authUserDetails);
//
//        //存入redis
//        redisUtil.set("token_" + Username, accessToken, 480, TimeUnit.MINUTES);
//
//        Map<String, String> map = new HashMap<>();
//        map.put("accessToken", accessToken);
//
//        ResponseUtils.out(response, JSONUtil.toJsonStr(ResponseUtils.success(map)));
//    }
//
//}

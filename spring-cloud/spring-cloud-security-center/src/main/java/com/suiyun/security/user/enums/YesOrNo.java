package com.suiyun.security.user.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum YesOrNo {
    /**
     * NO
     */
    NO(0,"fail"),
    /**
     * YES
     */
    YES(1,"success");

    private final int value;
    private final String valueStr;

}

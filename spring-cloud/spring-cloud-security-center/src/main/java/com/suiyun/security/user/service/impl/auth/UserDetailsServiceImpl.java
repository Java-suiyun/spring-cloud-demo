package com.suiyun.security.user.service.impl.auth;

import cn.hutool.core.collection.CollectionUtil;
import com.suiyun.security.user.model.PermissionInfo;
import com.suiyun.security.user.model.RoleInfo;
import com.suiyun.security.user.model.User;
import com.suiyun.security.user.service.PermissionInfoService;
import com.suiyun.security.user.service.RoleInfoService;
import com.suiyun.security.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author ww
 * @date 2020/9/26 下午3:56
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    private UserService userService;
    @Autowired
    private RoleInfoService roleInfoService;
    @Autowired
    private PermissionInfoService permissionInfoService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user= userService.findAuthUserByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("用户:" + username + ",不存在!");
        }
        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
        boolean enabled = true; // 可用性 :true:可用 false:不可用
        boolean accountNonExpired = true; // 过期性 :true:没过期 false:过期
        boolean credentialsNonExpired = true; // 有效性 :true:凭证有效 false:凭证无效
        boolean accountNonLocked = true; // 锁定性 :true:未锁定 false:已锁定


        List<RoleInfo> roleList = this.roleInfoService.getRolesByUserId(user.getId());

        if (CollectionUtil.isNotEmpty(roleList)){
            for (RoleInfo roleInfo : roleList) {
                //角色必须是ROLE_开头，可以在数据库中设置
                GrantedAuthority grantedAuthority = new SimpleGrantedAuthority("ROLE_"+roleInfo.getName());
                grantedAuthorities.add(grantedAuthority);
            }
        }

        //获取权限
        List<PermissionInfo> permissionsList = this.permissionInfoService.getPermissionsByUserId(user.getId());
        if (CollectionUtil.isNotEmpty(permissionsList)){
            for (PermissionInfo permissionInfo : permissionsList) {
                GrantedAuthority authority = new SimpleGrantedAuthority(permissionInfo.getPath());
                grantedAuthorities.add(authority);
            }
        }

        org.springframework.security.core.userdetails.User securityUser = new org.springframework.security.core.userdetails.User(user.getName(), user.getPassword(),
                enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, grantedAuthorities);
        return securityUser;
    }
}

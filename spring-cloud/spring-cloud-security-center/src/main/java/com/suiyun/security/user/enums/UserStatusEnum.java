package com.suiyun.security.user.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author ww
 * @date 2020/9/23 下午3:10
 */
@AllArgsConstructor
@Getter
public enum UserStatusEnum {

    /**
     * 禁用
     */
    FORBIDDEN(-1),
    /**
     * 正常
     */
    NORMAL(1);

    /**
     * 出现先后顺序
     */
    private final Integer value;
}

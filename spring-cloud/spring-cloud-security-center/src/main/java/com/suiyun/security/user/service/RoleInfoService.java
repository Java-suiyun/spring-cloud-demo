package com.suiyun.security.user.service;

import com.suiyun.security.user.model.RoleInfo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ww
 * @since 2020-09-22
 */
public interface RoleInfoService extends IService<RoleInfo> {

    List<RoleInfo> getRolesByUserId(Long id);

}

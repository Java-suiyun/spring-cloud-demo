package com.suiyun.security.user.service.impl;

import com.suiyun.security.user.model.RoleInfo;
import com.suiyun.security.user.mapper.RoleInfoMapper;
import com.suiyun.security.user.service.RoleInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ww
 * @since 2020-09-22
 */
@Service
public class RoleInfoServiceImpl extends ServiceImpl<RoleInfoMapper, RoleInfo> implements RoleInfoService {

    @Autowired
    private RoleInfoMapper roleInfoMapper;

    /**
     * 获取用户角色信息
     */
    @Override
    public List<RoleInfo> getRolesByUserId(Long id){
        return this.roleInfoMapper.getRolesByUserId(id);
    }

}

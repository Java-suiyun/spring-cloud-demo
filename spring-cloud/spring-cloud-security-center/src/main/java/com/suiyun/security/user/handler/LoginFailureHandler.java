//package com.suiyun.security.user.handler;
//
//import cn.hutool.json.JSONUtil;
//import com.suiyun.common.util.ResponseUtils;
//import com.suiyun.common.util.redis.RedisUtil;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.security.authentication.BadCredentialsException;
//import org.springframework.security.authentication.DisabledException;
//import org.springframework.security.core.AuthenticationException;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.security.web.authentication.AuthenticationFailureHandler;
//import org.springframework.stereotype.Component;
//
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//import java.util.concurrent.TimeUnit;
//
///**
// * 登陆失败处理过滤器
// * @author ww
// * @date 2020/9/23 下午4:52
// */
//@Component
//public class LoginFailureHandler implements AuthenticationFailureHandler {
//
//    //#限制用户登陆错误次数（次）
//    @Value("${security.loginTimeLimit}")
//    private Integer loginTimeLimit;
//    //#错误超过次数后多少分钟后才能继续登录（分钟）
//    @Value("${security.loginAfterTime}")
//    private Integer loginAfterTime;
//
//    @Autowired
//    private RedisUtil redisUtil;
//
//    /**
//     * 用户登陆失败处理类  记录用户登陆错误次数
//     */
//    @Override
//    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException e) throws IOException, ServletException {
//        if (e instanceof UsernameNotFoundException || e instanceof BadCredentialsException) {
//            String username = request.getParameter("username");
//            recordLoginTime(username);
//
//            ResponseUtils.out(response, JSONUtil.toJsonStr(ResponseUtils.failed("用户名或密码错误")));
//        } else if (e instanceof DisabledException) {
//            ResponseUtils.out(response,JSONUtil.toJsonStr(ResponseUtils.failed("账户被禁用，请联系管理员")));
//        } else {
//            ResponseUtils.out(response,JSONUtil.toJsonStr(ResponseUtils.failed("登录失败")));
//        }
//    }
//    /**
//     * 判断用户登陆错误次数
//     */
//    public boolean recordLoginTime(String username) {
//
//        String key = "loginTimeLimit:" + username;
//        String flagKey = "loginFailFlag:" + username;
//        Object value = redisUtil.get(key);
//        if (value == null) {
//            value = "0";
//        }
//        //获取已登录错误次数
//        int loginFailTime = Integer.valueOf(String.valueOf(value)) + 1;
//        redisUtil.set(key, String.valueOf(loginFailTime), loginAfterTime, TimeUnit.MINUTES);
//        if (loginFailTime >= loginTimeLimit) {
//
//            redisUtil.set(flagKey, "fail", loginAfterTime, TimeUnit.MINUTES);
//            return false;
//        }
//        return true;
//    }
//
//}
//

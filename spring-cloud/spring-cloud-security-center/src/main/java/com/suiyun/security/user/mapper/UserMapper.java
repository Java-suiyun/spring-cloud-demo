package com.suiyun.security.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.suiyun.security.user.model.User;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ww
 * @since 2020-09-22
 */
public interface UserMapper extends BaseMapper<User> {

}

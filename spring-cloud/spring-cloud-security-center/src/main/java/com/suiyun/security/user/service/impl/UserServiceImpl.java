package com.suiyun.security.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.suiyun.security.user.mapper.UserMapper;
import com.suiyun.security.user.model.User;
import com.suiyun.security.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ww
 * @since 2020-09-22
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Autowired
    private UserMapper userMapper;

    /**
     * 获取用户基本信息
     */
    @Override
    public User findAuthUserByUsername(String username) {
        User user = this.userMapper.selectOne(new QueryWrapper<User>()
                .select("id","name", "password","status")
                .eq("name", username)
        );
        return user;
    }

}

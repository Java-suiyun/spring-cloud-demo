package com.suiyun.security.user.mapper;

import com.suiyun.security.user.model.RoleInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ww
 * @since 2020-09-22
 */
public interface RoleInfoMapper extends BaseMapper<RoleInfo> {

    List<RoleInfo> getRolesByUserId(Long id);
}

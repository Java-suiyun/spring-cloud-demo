package com.suiyun.security.user.service.impl;

import com.suiyun.security.user.model.UserRole;
import com.suiyun.security.user.mapper.UserRoleMapper;
import com.suiyun.security.user.service.UserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ww
 * @since 2020-09-22
 */
@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements UserRoleService {

}

//package com.suiyun.security.user.handler;
//
//import cn.hutool.json.JSONUtil;
//import com.suiyun.common.enums.ReturnCodeEnum;
//import com.suiyun.common.util.ResponseUtils;
//import org.springframework.security.access.AccessDeniedException;
//import org.springframework.security.web.access.AccessDeniedHandler;
//import org.springframework.stereotype.Component;
//
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//
///**
// * 暂无权限处理过滤器
// * @author ww
// * @date 2020/9/24 上午10:14
// */
//@Component
//public class RestAccessDeniedHandler implements AccessDeniedHandler {
//
//    @Override
//    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException)
//            throws IOException, ServletException {
//        ResponseUtils.out(response, JSONUtil.toJsonStr(ResponseUtils.failed(ReturnCodeEnum.FORBIDDEN.getCode(),"您没有权限")));
//    }
//
//}
//

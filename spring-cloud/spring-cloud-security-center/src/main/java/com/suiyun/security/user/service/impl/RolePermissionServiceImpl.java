package com.suiyun.security.user.service.impl;

import com.suiyun.security.user.model.RolePermission;
import com.suiyun.security.user.mapper.RolePermissionMapper;
import com.suiyun.security.user.service.RolePermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ww
 * @since 2020-09-22
 */
@Service
public class RolePermissionServiceImpl extends ServiceImpl<RolePermissionMapper, RolePermission> implements RolePermissionService {

}

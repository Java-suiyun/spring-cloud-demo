//package com.suiyun.security.user.handler;
//
//import cn.hutool.json.JSONUtil;
//import com.suiyun.common.util.ResponseUtils;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
//import org.springframework.stereotype.Component;
//
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//
///**
// * 登出成功处理过滤器
// * @author ww
// * @date 2020/9/24 下午4:16
// */
//@Component
//public class UserLogoutSuccessHandler implements LogoutSuccessHandler {
//
//    @Override
//    public void onLogoutSuccess(HttpServletRequest httpServletRequest, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
//        SecurityContextHolder.clearContext();
//        //todo 清除token
//        ResponseUtils.out(response, JSONUtil.toJsonStr(ResponseUtils.failed("用户名或密码错误")));
//    }
//}

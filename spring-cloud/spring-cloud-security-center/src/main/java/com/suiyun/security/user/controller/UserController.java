package com.suiyun.security.user.controller;

import com.suiyun.common.ResponseData;
import com.suiyun.common.util.ResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.token.ConsumerTokenServices;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

/**
 * @author ww
 * @date 2020/9/26 下午4:23
 */
@RestController
public class UserController {
    @Autowired
    private ConsumerTokenServices consumerTokenServices;

    @RequestMapping("/user")
    public Principal user(Principal user) {
        return user;
    }

    @DeleteMapping(value = "/exit")
    @ResponseBody
    public ResponseData revokeToken(String accessToken){
        if (consumerTokenServices.revokeToken(accessToken)){
           return ResponseUtils.success("注销成功");
        }

       return ResponseUtils.failed("注销失败");
    }
}
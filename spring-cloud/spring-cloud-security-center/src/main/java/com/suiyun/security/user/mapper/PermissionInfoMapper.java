package com.suiyun.security.user.mapper;

import com.suiyun.security.user.model.PermissionInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ww
 * @since 2020-09-22
 */
public interface PermissionInfoMapper extends BaseMapper<PermissionInfo> {

    List<PermissionInfo> getPermissionsByUserId(Long id);
}

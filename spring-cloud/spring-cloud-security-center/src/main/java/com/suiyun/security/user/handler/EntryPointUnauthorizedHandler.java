//package com.suiyun.security.user.handler;
//
//import cn.hutool.json.JSONUtil;
//import com.suiyun.common.enums.ReturnCodeEnum;
//import com.suiyun.common.util.ResponseUtils;
//import org.springframework.security.core.AuthenticationException;
//import org.springframework.security.web.AuthenticationEntryPoint;
//import org.springframework.stereotype.Component;
//
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//
///**
// * 用户未登录处理过滤器
// * @author ww
// * @date 2020/9/24 上午10:21
// */
//@Component
//public class EntryPointUnauthorizedHandler implements AuthenticationEntryPoint {
//    @Override
//    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
//        ResponseUtils.out(response, JSONUtil.toJsonStr(ResponseUtils.failed(ReturnCodeEnum.FORBIDDEN.getCode(),"请登录后操作")));
//    }
//}
//

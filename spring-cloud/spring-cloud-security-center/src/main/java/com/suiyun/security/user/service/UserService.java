package com.suiyun.security.user.service;

import com.suiyun.security.user.model.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ww
 * @since 2020-09-22
 */
public interface UserService extends IService<User> {

    User findAuthUserByUsername(String username);
}

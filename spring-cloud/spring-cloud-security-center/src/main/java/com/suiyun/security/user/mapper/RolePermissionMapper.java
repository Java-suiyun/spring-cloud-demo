package com.suiyun.security.user.mapper;

import com.suiyun.security.user.model.RolePermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ww
 * @since 2020-09-22
 */
public interface RolePermissionMapper extends BaseMapper<RolePermission> {

}

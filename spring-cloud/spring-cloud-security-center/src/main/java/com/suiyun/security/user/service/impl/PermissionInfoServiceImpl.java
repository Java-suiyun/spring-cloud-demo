package com.suiyun.security.user.service.impl;

import com.suiyun.security.user.model.PermissionInfo;
import com.suiyun.security.user.mapper.PermissionInfoMapper;
import com.suiyun.security.user.service.PermissionInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ww
 * @since 2020-09-22
 */
@Service
public class PermissionInfoServiceImpl extends ServiceImpl<PermissionInfoMapper, PermissionInfo> implements PermissionInfoService {

    @Autowired
    private PermissionInfoMapper permissionInfoMapper;

    @Override
    public List<PermissionInfo> getPermissionsByUserId(Long id){
        return this.permissionInfoMapper.getPermissionsByUserId(id);
    }
}

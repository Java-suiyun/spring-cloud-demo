package com.suiyun.security.user.enums;

/**
 * @author ww
 * @date 2020/9/23 下午4:29
 */
public enum ScopesEnum {
    REFRESH_TOKEN;

    public String authority() {
        return "ROLE_" + this.name();
    }
}

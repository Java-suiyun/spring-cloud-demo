package com.suiyun.security;

import com.suiyun.common.util.redis.RedisUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringCloudSecurityCenterApplicationTests {

	@Autowired
	private RedisUtil redisUtil;

	@Test
	public void contextLoads() {
        redisUtil.set("loginFailFlag:123",1);

        Object o = redisUtil.get("loginFailFlag:123");
        System.out.println("vale========="+o);
	}

}

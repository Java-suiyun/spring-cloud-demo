package com.suiyun.order.controller;

import com.suiyun.common.ResponseData;
import com.suiyun.order.feign.product.ProductFeignToOrderClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 消费服务
 * @author ww
 */
@RestController
public class OrderFeignTestController {

    @Autowired(required = false)
    private ProductFeignToOrderClient productFeignToOrderClient;

    @GetMapping("/fromProduct")
    public ResponseData<String> fromProduct(){
        return this.productFeignToOrderClient.product();
    }

}

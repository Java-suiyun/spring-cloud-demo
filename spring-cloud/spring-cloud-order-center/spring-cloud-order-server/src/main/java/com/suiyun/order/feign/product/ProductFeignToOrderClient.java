package com.suiyun.order.feign.product;

import com.suiyun.common.ResponseData;
import com.suiyun.order.fallback.product.ProductFeignToOrderFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * 使用Feign调用接口 order -> product
 * @author ww
 */

@FeignClient(value = "cloud-product",fallback = ProductFeignToOrderFallback.class)
public interface ProductFeignToOrderClient {

    @GetMapping("/product")
    ResponseData<String> product();
}

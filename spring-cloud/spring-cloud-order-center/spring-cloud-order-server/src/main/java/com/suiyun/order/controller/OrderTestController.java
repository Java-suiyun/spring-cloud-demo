package com.suiyun.order.controller;

import com.suiyun.common.ResponseData;
import com.suiyun.common.util.ResponseUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 提供服务
 * @author ww
 */
@RestController
public class OrderTestController {

    @Value("${server.port}")
    private String port;


    @GetMapping("/order")
    public ResponseData<String> order(){
        return ResponseUtils.success("Hello！I'm order. port：" + port );
    }



}

package com.suiyun.order.fallback.product;

import com.suiyun.common.ResponseData;
import com.suiyun.common.util.ResponseUtils;
import com.suiyun.order.feign.product.ProductFeignToOrderClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author ww
 */
@Slf4j
@Component
public class ProductFeignToOrderFallback implements ProductFeignToOrderClient {

    @Override
    public ResponseData<String> product() {
        log.info("ProductFeignToOrderFallback.product 异常发生");
        return ResponseUtils.failed("ProductFeignToOrderFallback.product 异常发生");
    }
}

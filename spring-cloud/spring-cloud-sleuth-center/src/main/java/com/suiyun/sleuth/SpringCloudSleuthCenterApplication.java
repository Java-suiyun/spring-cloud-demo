package com.suiyun.sleuth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import zipkin2.server.internal.EnableZipkinServer;

@EnableEurekaClient
@EnableZipkinServer
@SpringBootApplication
public class SpringCloudSleuthCenterApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringCloudSleuthCenterApplication.class, args);
	}

}

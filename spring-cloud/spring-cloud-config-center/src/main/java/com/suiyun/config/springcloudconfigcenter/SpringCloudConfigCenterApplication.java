package com.suiyun.config.springcloudconfigcenter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class SpringCloudConfigCenterApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringCloudConfigCenterApplication.class, args);
	}

}
